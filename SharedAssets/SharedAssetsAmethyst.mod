<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SharedAssetsAmethyst" version="1.0" date="30/12/2008" >

		<Author name="talvinen" email="" />
		<Description text="Textures used by Amethyst (and possibly other addons) via LibSharedAssets." />
        
        <VersionSettings gameVersion="1.3.1" />
        
		<Files>
			<File name="LibStub.lua" />
			<File name="LibSharedAssets.lua" />
            <File name="textures\textures.xml" />
			<File name="textures\textures.lua" />
		</Files>
		
		<OnInitialize/>
		<OnUpdate/>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>
