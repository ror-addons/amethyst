-- initialize textures
local LibSharedAssets = LibStub("LibSharedAssets")
local textureList = LibSharedAssets:GetTextureList("statusbar")
table.sort(textureList)



local LibGUI = LibStub("LibGUI")
Amethyst = {}

local DEFAULTSETTINGS = {
	version = 1.41,
	width = 350,
	height = 30,
	border = 4,
	scale = 0.7,
	x = 450,
	y = 590,
	font = "font_journal_text",
	texture = "SharedMediaBantoBar",
	texdimensions = {
		256,
		32,
		},
	accurate = true,
	alpha = {
		background = 0.2,
		foreground = 1,
		},
	color = {
		background = {50,50,50},
		foreground = {103,0,154},
		success = {0,255,0},
		cancel = {255,0,0},
		},
	name = {
		x = 10,
		y = 3,
		},
	timer = {
		x = -10,
		y = 3,
		},
	icon = {
		x = 0,
		y = 0,
		position = "left",
		},
	}

local CastData
local C, GUI

function Amethyst.FrameHandle(what)
	return C
end

-- adds the values of tab2 to tab1, if not present
local function addtable(tab1, tab2)
	for key,val in pairs(tab2) do
		if (type(val) == "table") then
			if (not tab1[key] or type(tab1[key]) ~= "table") then
				tab1[key] = {}
			end
			addtable(tab1[key],val)
		end
		if (not tab1[key]) then
			tab1[key] = val
		end
	end
end

local function round(i,n)
	return string.format("%."..n.."f",i)
end

local function SetColor(index)
	C.Fill:Tint(Amethyst.Settings.color[index])
end

local function SetValue(a, b)
	s = Amethyst.Settings
	C.Fill:Resize(s.width * a/b, s.height)
	C.Fill:TexDims(s.texdimensions[1] * a/b, s.texdimensions[2])
	
end

function Amethyst.StartInteractTimer()
	CastData = {}
	CastData.current = 0
	CastData.maximum = GameData.InteractTimer.time
	CastData.spell = GameData.InteractTimer.objectName
	CastData.desired = GameData.InteractTimer.time
	CastData.channel = false
			
	SetColor("foreground")
	WindowStopAlphaAnimation(C.name)
	C:Alpha(1)
	C.Name:SetText(CastData.spell)
	C.Icon:Hide()
	C:Show()
end

function Amethyst.HideCastBar(isCancel)
	if (CastData) then
		CastData = nil
		SetColor((isCancel and "cancel") or "success")
		WindowStartAlphaAnimation(C.name, Window.AnimationType.SINGLE_NO_RESET, 1, 0, 0.5, false, 0, 0)
	end
end

function Amethyst.ShowCastBar(abilityId, isChannel, desiredCastTime, averageLatency)
	if (desiredCastTime == 0) then
		return
	end
	
	local abilityData = GetAbilityData(abilityId)
	local texture = GetIconData(abilityData.iconNum)

	if (CastData and CastData.current > 1 and CastData.current < desiredCastTime) then
		CastData.maximum = desiredCastTime + (desiredCastTime - CastData.current)
	else
		CastData = {}
		CastData.current = 0
		CastData.maximum = desiredCastTime
		CastData.spell = abilityData.name
		CastData.channel = isChannel
	end
	
	SetColor("foreground")
	WindowStopAlphaAnimation(C.name)
	C.Name:SetText(CastData.spell)
	C:Alpha(1)
	if (texture ~= "icon000000") then
		C.Icon:Show()
		C.Icon:Texture(texture)
	else
		C.Icon:Hide()
	end
	C:Show()
end

function Amethyst.SetbackCastBar(new)
	if (CastData) then
		CastData.current = CastData.maximum - new
		if (CastData.current < 0) then
			CastData.current = 0
		end
	end
end

function Amethyst.OnUpdate(elapsed)
	if (not C) then
		Amethyst.Recreate()
	end
	
	if (GUI and GUI.Colorizer:Showing()) then
		WindowSetTintColor(GUI.Colorizer.Object.name,
											 GUI.Colorizer.R:GetValue(),
											 GUI.Colorizer.G:GetValue(),
											 GUI.Colorizer.B:GetValue())
	end

	if (CastData) then
		CastData.current = CastData.current + elapsed
		
		-- if we're 5 seconds over time, most certainly something went wrong
		if (CastData.current - CastData.maximum > 5) then
			Amethyst.HideCastBar()
			return
		end
		
		local a = (CastData.channel and (CastData.maximum - CastData.current)) or CastData.current
		local b = CastData.maximum
		local s = Amethyst.Settings

		C.Timer:SetText(string.format("%.1f/%.1f",a,b))

		if (a > b or a < 0) then
			C.Fill:Resize(s.width, s.height)
			if (s.accurate) then
				Amethyst.HideCastBar(false)
			end
		else
			SetValue(a, b)
		end
	end
end

function Amethyst.SavePosition()
	if (not C) then
		return
	end

	local x, y = WindowGetScreenPosition(C.name)
	local w, h = WindowGetDimensions(C.name)
	local border = Amethyst.Settings.border
	Amethyst.Settings.x = x
	Amethyst.Settings.y = y
	Amethyst.Settings.scale = WindowGetScale(C.name)
	Amethyst.Settings.height = h - border
	Amethyst.Settings.width = w - border
	C.height = h
	C.width = w
	C.Fill.height = h - border
end

function Amethyst.Initialize()
	EA_ChatWindow.Print(L"Amethyst Initialized. Configuration via /amt")
	LibSlash.RegisterSlashCmd("amt",function(msg) Amethyst.Slash(msg) end)
	LibSlash.RegisterSlashCmd("amethyst",function(msg) Amethyst.Slash(msg) end)

	table.insert(LayoutEditor.EventHandlers, Amethyst.SavePosition)
		
	-- replace the default castbar functions
	LayerTimerWindow.StartInteractTimer = Amethyst.StartInteractTimer
	LayerTimerWindow.HideCastBar = Amethyst.HideCastBar
	LayerTimerWindow.ShowCastBar = Amethyst.ShowCastBar
	LayerTimerWindow.SetbackCastBar = Amethyst.SetbackCastBar
	LayerTimerWindow.Update = function(elapsed) end

	if (not Amethyst.Settings) then
		Amethyst.Settings = DEFAULTSETTINGS
	end
	if (Amethyst.Settings.version < DEFAULTSETTINGS.version) then
		addtable(Amethyst.Settings, DEFAULTSETTINGS)
	end
end

function Amethyst.Recreate()
	local s = Amethyst.Settings
	local scale = InterfaceCore.GetScale()

	if (C) then
		LayoutEditor.UnregisterWindow(C.name)
		C:Destroy()
	end

	C = LibGUI("Window",nil,"TooltipBase")
	C:Resize(s.width + s.border, s.height + s.border)
	C:Position(s.x/scale, s.y/scale)
	C:Hide()
	C:IgnoreInput()
	LayoutEditor.RegisterWindow(C.name,L"Amethyst CastBar","",true,true,true)
	
	C.Frame = C("Image")
	C.Frame:AnchorTo(C)
	C.Frame:AddAnchor(C,"bottomright","bottomright")
	C.Frame:Alpha(s.alpha.background)
	WindowSetTintColor(C.Frame.name,unpack(s.color.background))
	
	C.Fill = C("Image")
	C.Fill:Resize(s.width, s.height)
	C.Fill:AnchorTo(C, "left", "left", s.border)
	C.Fill:Alpha(s.alpha.foreground)
	C.Fill:Texture(s.texture)
	C.Fill:TexSlice("CastBar")
	C.Fill:TexDims(unpack(s.texdimensions))
	
	C.Name = C("Label")
	C.Name:Resize(300)
	C.Name:AnchorTo(C, "left", "left", s.name.x, s.name.y)
	C.Name:Font(s.font)
	C.Name:Align("left")
	C.Name:Color(255,255,255)
	
	C.Timer = C("Label")
	C.Timer:Resize(100)
	C.Timer:AnchorTo(C, "right", "right", s.timer.x, s.timer.y)
	C.Timer:Font(s.font)
	C.Timer:Align("right")
	C.Timer:Color(255,255,255)
	
	C.Icon = C("Image")
	C.Icon:Resize(s.height + s.border, s.height + s.border)
	if (s.icon.position == "left") then
		C.Icon:AnchorTo(C, "left", "right", s.icon.x, s.icon.y)
	elseif (s.icon.position == "right") then
		C.Icon:AnchorTo(C, "right", "left", s.icon.x, s.icon.y)
	elseif (s.icon.position == "none") then
		C.Icon:Resize(0,0)
	end
	C.Icon:TexDims(64,64)
	
	WindowSetScale(C.name, s.scale)
	WindowSetScale(C.Fill.name, s.scale)
	WindowSetScale(C.Name.name, s.scale)
	WindowSetScale(C.Timer.name, s.scale)
	
end

function Amethyst.ShowDummyCastBar()
    local s = Amethyst.Settings
	WindowStopAlphaAnimation(C.name)
	C:Alpha(1)
	C.Name:SetText("Amethyst CastBar")
	texture = GetIconData(123)
	C.Icon:Texture(texture)
	C.Icon:Show()
	local a = 2.1
	local b = 2.5
	C.Timer:SetText(string.format("%.1f/%.1f",a,b))
	SetValue(a, b)
	C:Show()
	SetColor("foreground")
end

function Amethyst.Slash()
	if (not GUI) then
		Amethyst.CreateGUI()
	end
	
	local s = Amethyst.Settings
	local I = GUI.Input
	
	I.Width:SetText(round(s.width, 0))
	I.Height:SetText(round(s.height, 0))
	I.Scale:SetText(round(s.scale, 2))
	I.Bordersize:SetText(s.border)
	
	I.IconX:SetText(s.icon.x)
	I.IconY:SetText(s.icon.y)
	I.IconPosition:Select(s.icon.position)
	
	I.BarAlpha:SetText(s.alpha.foreground)
	I.BgAlpha:SetText(s.alpha.background)
	
	WindowSetTintColor(I.BgColor.Overlay.name, unpack(s.color.background))
	WindowSetTintColor(I.BarColor.Overlay.name, unpack(s.color.foreground))
	WindowSetTintColor(I.SuccessColor.Overlay.name, unpack(s.color.success))
	WindowSetTintColor(I.CancelColor.Overlay.name, unpack(s.color.cancel))
	
	I.NameX:SetText(s.name.x)
	I.NameY:SetText(s.name.y)

	I.TimerX:SetText(s.timer.x)
	I.TimerY:SetText(s.timer.y)
	
	I.Accurate:SetValue(s.accurate)
	I.Font:Select(s.font)
	I.Texture:Select(s.texture)
	
	GUI:Show()
    Amethyst.ShowDummyCastBar()
end

function Amethyst.ApplySettings()
	local s = Amethyst.Settings
	local I = GUI.Input
	
	s.width = tonumber(I.Width:GetText())
	s.height = tonumber(I.Height:GetText())
	s.scale = tonumber(I.Scale:GetText())
	s.border = tonumber(I.Bordersize:GetText())
	
	s.icon.x = tonumber(I.IconX:GetText())
	s.icon.y = tonumber(I.IconY:GetText())
	s.icon.position = WStringToString(I.IconPosition:Selected())

	s.alpha.foreground = tonumber(I.BarAlpha:GetText())
	s.alpha.background = tonumber(I.BgAlpha:GetText())
	
	s.color.background = {WindowGetTintColor(I.BgColor.Overlay.name)}
	s.color.foreground = {WindowGetTintColor(I.BarColor.Overlay.name)}
	s.color.success = {WindowGetTintColor(I.SuccessColor.Overlay.name)}
	s.color.cancel = {WindowGetTintColor(I.CancelColor.Overlay.name)}

	s.name.x = tonumber(I.NameX:GetText())
	s.name.y = tonumber(I.NameY:GetText())

	s.timer.x = tonumber(I.TimerX:GetText())
	s.timer.y = tonumber(I.TimerY:GetText())
	
	s.accurate = I.Accurate:GetValue()
	s.font = WStringToString(I.Font:Selected())
	s.texture = WStringToString(I.Texture:Selected())
	s.texdimensions = LibSharedAssets:GetMetadata(s.texture).size
	
	EA_ChatWindow.Print(L"Amethyst: Settings applied.")
	
	Amethyst.Recreate()
	Amethyst.ShowDummyCastBar()
end

function Amethyst.CreateGUI()
	GUI = LibGUI("Blackframe")
	GUI:MakeMovable()
	GUI:AnchorTo("Root","center","center")
	GUI:Resize(800,500)
	
	GUI.CloseButton = GUI("Closebutton")
	GUI.CloseButton.OnLButtonUp =
		function()
			GUI:Hide()
		end
		
	GUI.Title = GUI("Label")
	GUI.Title:Resize(200)
	GUI.Title:AnchorTo(GUI, "top", "top", 0, 10)
	GUI.Title:Font("font_clear_large_bold")
	GUI.Title:SetText("Amethyst")
	GUI.Title:Color(153,50,204)
	GUI.Title:Align("center")
	GUI.Title:IgnoreInput()

	GUI.Apply = GUI("Button")
	GUI.Apply:Resize(250)
	GUI.Apply:SetText("Apply")
	GUI.Apply:AnchorTo(GUI,"bottom","bottomright",-20,-20)
	GUI.Apply.OnLButtonUp =
		function()
			Amethyst.ApplySettings()
		end
		
	GUI.Cancel = GUI("Button")
	GUI.Cancel:Resize(250)
	GUI.Cancel:SetText("Cancel")
	GUI.Cancel:AnchorTo(GUI,"bottom","bottomleft",20,-20)
	GUI.Cancel.OnLButtonUp =
		function()
			GUI:Hide()
		end
	
	GUI.Label = {}
	GUI.Input = {}
	
	-- 1st column

	GUI.Label.Size = GUI("Label")
	GUI.Label.Size:Position(20,55)
	GUI.Label.Size:Align("left")
	GUI.Label.Size:SetText("size:")
	
	GUI.Input.Width = GUI("Textbox")
	GUI.Input.Width:Resize(100)
	GUI.Input.Width:Position(20,80)
	GUI.Label.Width = GUI("Label")
	GUI.Label.Width:AnchorTo(GUI.Input.Width, "right", "left", 15, 5)
	GUI.Label.Width:Align("left")
	GUI.Label.Width:SetText("width")
	
	GUI.Input.Height = GUI("Textbox")
	GUI.Input.Height:Resize(100)
	GUI.Input.Height:Position(20,120)
	GUI.Label.Height = GUI("Label")
	GUI.Label.Height:AnchorTo(GUI.Input.Height, "right", "left", 15, 5)
	GUI.Label.Height:Align("left")
	GUI.Label.Height:SetText("height")

	GUI.Input.Bordersize = GUI("Textbox")
	GUI.Input.Bordersize:Resize(100)
	GUI.Input.Bordersize:Position(20,160)
	GUI.Label.Bordersize = GUI("Label")
	GUI.Label.Bordersize:AnchorTo(GUI.Input.Bordersize, "right", "left", 15, 5)
	GUI.Label.Bordersize:Align("left")
	GUI.Label.Bordersize:SetText("border size")
	
	GUI.Input.Scale = GUI("Textbox")
	GUI.Input.Scale:Resize(100)
	GUI.Input.Scale:Position(20,200)
	GUI.Label.Scale = GUI("Label")
	GUI.Label.Scale:AnchorTo(GUI.Input.Scale, "right", "left", 15, 5)
	GUI.Label.Scale:Align("left")
	GUI.Label.Scale:SetText("scale")
	
	GUI.Label.IconOffset = GUI("Label")
	GUI.Label.IconOffset:Position(20,255)
	GUI.Label.IconOffset:Align("left")
	GUI.Label.IconOffset:SetText("icon offset:")

	GUI.Input.IconX = GUI("Textbox")
	GUI.Input.IconX:Resize(100)
	GUI.Input.IconX:Position(20,280)
	GUI.Label.IconX = GUI("Label")
	GUI.Label.IconX:AnchorTo(GUI.Input.IconX, "right", "left", 15, 5)
	GUI.Label.IconX:Align("left")
	GUI.Label.IconX:SetText("x")
	
	GUI.Input.IconY = GUI("Textbox")
	GUI.Input.IconY:Resize(100)
	GUI.Input.IconY:Position(20,320)
	GUI.Label.IconY = GUI("Label")
	GUI.Label.IconY:AnchorTo(GUI.Input.IconY, "right", "left", 15, 5)
	GUI.Label.IconY:Align("left")
	GUI.Label.IconY:SetText("y")

	GUI.Input.IconPosition = GUI("smallcombobox")
	GUI.Input.IconPosition:Position(20,360)
	GUI.Label.IconPosition = GUI("Label")
	GUI.Label.IconPosition:AnchorTo(GUI.Input.IconPosition, "right", "left", 15, 5)
	GUI.Label.IconPosition:Align("left")
	GUI.Label.IconPosition:SetText("position")
	GUI.Input.IconPosition:Add("left")
	GUI.Input.IconPosition:Add("right")
	GUI.Input.IconPosition:Add("none")
	
	
	-- 2nd column

	GUI.Label.Alpha = GUI("Label")
	GUI.Label.Alpha:Position(300,55)
	GUI.Label.Alpha:Align("left")
	GUI.Label.Alpha:SetText("alpha:")

	GUI.Input.BarAlpha = GUI("Textbox")
	GUI.Input.BarAlpha:Resize(100)
	GUI.Input.BarAlpha:Position(300,80)
	GUI.Label.BarAlpha = GUI("Label")
	GUI.Label.BarAlpha:AnchorTo(GUI.Input.BarAlpha, "right", "left", 15, 5)
	GUI.Label.BarAlpha:Align("left")
	GUI.Label.BarAlpha:SetText("foreground")
	
	GUI.Input.BgAlpha = GUI("Textbox")
	GUI.Input.BgAlpha:Resize(100)
	GUI.Input.BgAlpha:Position(300,120)
	GUI.Label.BgAlpha = GUI("Label")
	GUI.Label.BgAlpha:AnchorTo(GUI.Input.BgAlpha, "right", "left", 15, 5)
	GUI.Label.BgAlpha:Align("left")
	GUI.Label.BgAlpha:SetText("background")

	GUI.Label.Color = GUI("Label")
	GUI.Label.Color:Position(300,175)
	GUI.Label.Color:Align("left")
	GUI.Label.Color:SetText("colors:")
	
	GUI.Input.BgColor = GUI("Button")
	GUI.Input.BgColor:Resize(36,36)
	GUI.Input.BgColor:Position(300,200)
	GUI.Input.BgColor:Alpha(0)
	GUI.Input.BgColor.Overlay = GUI("Image")
	GUI.Input.BgColor.Overlay:Resize(36,36)
	GUI.Input.BgColor.Overlay:Position(300,200)
	GUI.Input.BgColor.Overlay:IgnoreInput()
	GUI.Label.BgColor = GUI("Label")
	GUI.Label.BgColor:AnchorTo(GUI.Input.BgColor, "right", "left", 15, 5)
	GUI.Label.BgColor:Align("left")
	GUI.Label.BgColor:SetText("background")
	GUI.Input.BgColor.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.BgColor.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end
	
	GUI.Input.BarColor = GUI("Button")
	GUI.Input.BarColor:Resize(36,36)
	GUI.Input.BarColor:Position(300,240)
	GUI.Input.BarColor:Alpha(0)
	GUI.Input.BarColor.Overlay = GUI("Image")
	GUI.Input.BarColor.Overlay:Resize(36,36)
	GUI.Input.BarColor.Overlay:Position(300,240)
	GUI.Input.BarColor.Overlay:IgnoreInput()
	GUI.Label.BarColor = GUI("Label")
	GUI.Label.BarColor:AnchorTo(GUI.Input.BarColor, "right", "left", 15, 5)
	GUI.Label.BarColor:Align("left")
	GUI.Label.BarColor:SetText("foreground")
	GUI.Input.BarColor.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.BarColor.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end
	
	GUI.Input.SuccessColor = GUI("Button")
	GUI.Input.SuccessColor:Resize(36,36)
	GUI.Input.SuccessColor:Position(300,280)
	GUI.Input.SuccessColor:Alpha(0)
	GUI.Input.SuccessColor.Overlay = GUI("Image")
	GUI.Input.SuccessColor.Overlay:Resize(36,36)
	GUI.Input.SuccessColor.Overlay:Position(300,280)
	GUI.Input.SuccessColor.Overlay:IgnoreInput()
	GUI.Label.SuccessColor = GUI("Label")
	GUI.Label.SuccessColor:AnchorTo(GUI.Input.SuccessColor, "right", "left", 15, 5)
	GUI.Label.SuccessColor:Align("left")
	GUI.Label.SuccessColor:SetText("success")
	GUI.Input.SuccessColor.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.SuccessColor.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end
	
	GUI.Input.CancelColor = GUI("Button")
	GUI.Input.CancelColor:Resize(36,36)
	GUI.Input.CancelColor:Position(300,320)
	GUI.Input.CancelColor:Alpha(0)
	GUI.Input.CancelColor.Overlay = GUI("Image")
	GUI.Input.CancelColor.Overlay:Resize(36,36)
	GUI.Input.CancelColor.Overlay:Position(300,320)
	GUI.Input.CancelColor.Overlay:IgnoreInput()
	GUI.Label.CancelColor = GUI("Label")
	GUI.Label.CancelColor:AnchorTo(GUI.Input.CancelColor, "right", "left", 15, 5)
	GUI.Label.CancelColor:Align("left")
	GUI.Label.CancelColor:SetText("cancel")
	GUI.Input.CancelColor.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.CancelColor.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end
	
	-- 3rd column
	
	GUI.Label.NameOffset = GUI("Label")
	GUI.Label.NameOffset:Position(580,55)
	GUI.Label.NameOffset:Align("left")
	GUI.Label.NameOffset:SetText("spell name offset:")
	
	GUI.Input.NameX = GUI("Textbox")
	GUI.Input.NameX:Resize(100)
	GUI.Input.NameX:Position(580,80)
	GUI.Label.NameX = GUI("Label")
	GUI.Label.NameX:AnchorTo(GUI.Input.NameX, "right", "left", 15, 5)
	GUI.Label.NameX:Align("left")
	GUI.Label.NameX:SetText("x")

	GUI.Input.NameY = GUI("Textbox")
	GUI.Input.NameY:Resize(100)
	GUI.Input.NameY:Position(580,120)
	GUI.Label.NameY = GUI("Label")
	GUI.Label.NameY:AnchorTo(GUI.Input.NameY, "right", "left", 15, 5)
	GUI.Label.NameY:Align("left")
	GUI.Label.NameY:SetText("y")
	
	GUI.Label.TimerOffset = GUI("Label")
	GUI.Label.TimerOffset:Position(580,175)
	GUI.Label.TimerOffset:Align("left")
	GUI.Label.TimerOffset:SetText("timer offset:")
	
	GUI.Input.TimerX = GUI("Textbox")
	GUI.Input.TimerX:Resize(100)
	GUI.Input.TimerX:Position(580,200)
	GUI.Label.TimerX = GUI("Label")
	GUI.Label.TimerX:AnchorTo(GUI.Input.TimerX, "right", "left", 15, 5)
	GUI.Label.TimerX:Align("left")
	GUI.Label.TimerX:SetText("x")

	GUI.Input.TimerY = GUI("Textbox")
	GUI.Input.TimerY:Resize(100)
	GUI.Input.TimerY:Position(580,240)
	GUI.Label.TimerY = GUI("Label")
	GUI.Label.TimerY:AnchorTo(GUI.Input.TimerY, "right", "left", 15, 5)
	GUI.Label.TimerY:Align("left")
	GUI.Label.TimerY:SetText("y")
	
	GUI.Label.Behaviour = GUI("Label")
	GUI.Label.Behaviour:Position(580,295)
	GUI.Label.Behaviour:Align("left")
	GUI.Label.Behaviour:SetText("other:")
	
	GUI.Input.Accurate = GUI("Checkbox")
	GUI.Input.Accurate:Position(580,320)
	GUI.Label.Accurate = GUI("Label")
	GUI.Label.Accurate:AnchorTo(GUI.Input.Accurate, "right", "left", 15, 5)
	GUI.Label.Accurate:Align("left")
	GUI.Label.Accurate:SetText("compensate latency")
	
	GUI.Input.Font = GUI("combobox")
	GUI.Input.Font:Position(530,360)
	GUI.Input.Font:Add("font_journal_text")
	GUI.Input.Font:Add("font_default_text")
	GUI.Input.Font:Add("font_clear_medium_bold")
	GUI.Input.Font:Add("font_clear_small_bold")
	GUI.Input.Font:Add("font_clear_medium")
	GUI.Input.Font:Add("font_clear_small") 
	GUI.Input.Font:Add("font_chat_text")
	
	GUI.Input.Texture = GUI("combobox")
	GUI.Input.Texture:Position(530,400)
	for _, tex in ipairs(textureList) do
		GUI.Input.Texture:Add(tex)
	end


	GUI.Colorizer = GUI("Blackframe")
	GUI.Colorizer:Resize(250,250)
	GUI.Colorizer:Position(500,200)
	GUI.Colorizer:Hide()
	
	GUI.Colorizer.RLabel = GUI.Colorizer("Label")
	GUI.Colorizer.RLabel:SetText("red")
	GUI.Colorizer.RLabel:Position(20,0)
	
	GUI.Colorizer.R = GUI.Colorizer("Slider")
	GUI.Colorizer.R:Position(20,20)
	GUI.Colorizer.R:SetRange(0,255)
	
	GUI.Colorizer.GLabel = GUI.Colorizer("Label")
	GUI.Colorizer.GLabel:SetText("green")
	GUI.Colorizer.GLabel:Position(20,60)
	
	GUI.Colorizer.G = GUI.Colorizer("Slider")
	GUI.Colorizer.G:Position(20,80)
	GUI.Colorizer.G:SetRange(0,255)

	GUI.Colorizer.BLabel = GUI.Colorizer("Label")
	GUI.Colorizer.BLabel:SetText("blue")
	GUI.Colorizer.BLabel:Position(20,120)
	
	GUI.Colorizer.B = GUI.Colorizer("Slider")
	GUI.Colorizer.B:Position(20,140)
	GUI.Colorizer.B:SetRange(0,255)
	
	GUI.Colorizer.Okay = GUI.Colorizer("Button")
	GUI.Colorizer.Okay:SetText("Okay")
	GUI.Colorizer.Okay:Resize(200)
	GUI.Colorizer.Okay:AnchorTo(GUI.Colorizer, "bottom", "bottom", 0, -20)
	GUI.Colorizer.Okay.OnLButtonUp =
		function()
			GUI.Colorizer:Hide()
		end
	
	for k,v in pairs(GUI.Label) do
		v:IgnoreInput()
	end
	
	return GUI
end