<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="Amethyst" version="1.4.1" date="10/25/2008" >

    <Author name="Talvinen" email="redefiance@gmx.de" />
    <Description text="Fully customizable CastBar Replacement" />
    
    <Dependencies>
      <Dependency name="EA_CastTimerWindow" />
      <Dependency name="LibSlash" />
      <Dependency name="SharedAssetsAmethyst" />
    </Dependencies>

    <Files>
      <File name="LibStub.lua" />
      <File name="LibGUI.lua" />
      <File name="Amethyst.lua" />
    </Files>

    <SavedVariables>
      <SavedVariable name="Amethyst.Settings" />
    </SavedVariables>

    <OnInitialize>
      <CallFunction name="Amethyst.Initialize" />
    </OnInitialize>
    <OnUpdate>
      <CallFunction name="Amethyst.OnUpdate" />
    </OnUpdate>

  </UiMod>
</ModuleFile>
