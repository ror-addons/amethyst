<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="AmethystEffects" version="1.0" date="10/25/2008" >

    <Author name="Talvinen" email="redefiance@gmx.de" />
    <Description text="Adds timers for Buffs/Debuffs to Amethyst" />
    
    <Dependencies>
      <Dependency name="Amethyst" />
    </Dependencies>

    <Files>
      <File name="AmethystEffects.lua" />
    </Files>

    <SavedVariables>
      <SavedVariable name="AmethystEffects.Settings" />
    </SavedVariables>

    <OnInitialize>
      <CallFunction name="AmethystEffects.Initialize" />
    </OnInitialize>
    <OnUpdate>
      <CallFunction name="AmethystEffects.OnUpdate" />
    </OnUpdate>

  </UiMod>
</ModuleFile>
