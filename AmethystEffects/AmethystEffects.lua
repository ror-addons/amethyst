AmethystEffects = {}

local LibGUI = LibStub("LibGUI")

local DEFAULTSETTINGS = {
	version = 1.0,
	selffriendlytarget = {
		enabled = true,
		width = 150,
		height = 20,
		color = {0,0,180},
		anchor = "topleft",
		x = 0,
		y = 0,
		iconpos = "left",
	},
	selfhostiletarget = {
		enabled = true,
		width = 150,
		height = 20,
		color = {180,0,0},
		anchor = "topright",
		x = 0,
		y = 0,
		iconpos = "left",
	},
}

local durationTable = {

	}

local F = {
  selffriendlytarget = {},
	selfhostiletarget = {},
	}
local effects = {
	selffriendlytarget = {},
	selfhostiletarget = {},
	}
local old_targets = {}
local maximum = {
	selffriendlytarget = 0,
	selfhostiletarget = 0,
	}
local time_passed = 0
local GUI
local CreateGUIHook
local RecreateHook

function AmethystEffects.OnUpdate(elapsed)

	if (GUI and GUI.Colorizer:Showing()) then
		WindowSetTintColor(GUI.Colorizer.Object.name,
											 GUI.Colorizer.R:GetValue(),
											 GUI.Colorizer.G:GetValue(),
											 GUI.Colorizer.B:GetValue())
	end

	time_passed = time_passed + elapsed
	if (time_passed > 0.1) then
		for typ,_ in pairs(effects) do
			for k,v in pairs(effects[typ]) do
				v.duration = v.duration - time_passed
				if (v.duration <= 0) then
					effects[typ][k] = nil
				end
			end
			AmethystEffects.Render(typ)
		end
		time_passed = 0
	end
end

function AmethystEffects.Initialize()
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "AmethystEffects.UpdateTargets")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "AmethystEffects.UpdateEffects")
	
	if (not AmethystEffects.Settings) then
		AmethystEffects.Settings = DEFAULTSETTINGS
	end
	
	CreateGUIHook = Amethyst.CreateGUI
	RecreateHook = Amethyst.Recreate
	Amethyst.CreateGUI = AmethystEffects.CreateGUI
	Amethyst.Recreate = AmethystEffects.Recreate
end

function AmethystEffects.UpdateTargets(typ, targetId, targetType)
    local id

	if (typ == "selffriendlytarget") then
		id = 8
	elseif (typ == "selfhostiletarget") then
		id = 7
	else
		return
	end

	if (not AmethystEffects.Settings[typ].enabled) then
		return
	end
	
	TargetInfo:UpdateFromClient()
	if (TargetInfo:UnitEntityId(typ) == old_targets[typ]) then
		return
	end
	old_targets[typ] = TargetInfo:UnitEntityId(typ)

	effects[typ] = {}	

end

function AmethystEffects.UpdateEffects(updateType, buffs, isFull)
	local typ
	if (updateType == GameData.BuffTargetType.TARGET_FRIENDLY) then
		typ = "selffriendlytarget"
	else
		typ = "selfhostiletarget"
	end
	
	if (not AmethystEffects.Settings[typ].enabled) then
		return
	end
	
	for k,v in pairs(buffs) do
		if (v.name and v.castByPlayer and v.duration > 0 and v.duration < 300) then
			effects[typ][k] = {}
			effects[typ][k].data = v
			effects[typ][k].duration = v.duration
			if (not durationTable[v.name]) then
			    durationTable[v.name] = v.duration
			else
			    if (v.duration > durationTable[v.name]) then
			        durationTable[v.name] = v.duration
			    end
			end
		else
			effects[typ][k] = nil
		end
	end

	AmethystEffects.Render(typ)
end

function AmethystEffects.Render(typ)
	local i = 0
	local B
	
	for k,v in pairs(effects[typ]) do
		i = i + 1
		if (not F[typ][i]) then
			local s = AmethystEffects.Settings[typ]
			local growth
			if (s.anchor == "topleft" or s.anchor == "topright") then
				growth = -1
			else
				growth = 1
			end
			
			F[typ][i] = LibGUI("Window",nil,"TooltipBase")
			B = F[typ][i]
			B:Resize(s.width, s.height)
			B:IgnoreInput()
			B:AnchorTo(Amethyst.FrameHandle(),s.anchor,s.anchor,s.x,growth*s.height*i+s.y)
			
			B.Background = B("Image")
			B.Background:Tint(0,0,0)
			B.Background:Alpha(0.8)
			B.Background:Layer("background")
			B.Background:Resize(s.width, s.height)
			
			B.Fill = B("Image")
			B.Fill:Tint(s.color)
			B.Fill:Position(0,0)
			B.Fill:Texture("SharedMediaBantoBar")
			B.Fill:TexSlice("CastBar")
			B.Fill:TexDims(50,14)
			
			B.Name = B("Label")
			B.Name:Font("font_clear_small")
			B.Name:Align("left")
			B.Name:AnchorTo(B,"left","left",5,7)
			B.Name:Resize(s.width-40)
			B.Name:Color(255,255,255)
			B.Name:WordWrap(false)
			
			B.Timer = B("Label")
			B.Timer:Font("font_clear_small")
			B.Timer:Align("right")
			B.Timer:AnchorTo(B,"right","right",-5,7)
			B.Timer:Resize(40)
			B.Timer:Color(255,255,255)
			
			B.Icon = B("Image")
			B.Icon:Resize(s.height,s.height)
			if (s.iconpos == "left") then
				B.Icon:AnchorTo(B,"left","right")
			elseif (s.iconpos == "right") then
				B.Icon:AnchorTo(B,"right","left")
			else
				B.Icon:Resize(0,0)
			end
			B.Icon:TexDims(64,64)
		else
			B = F[typ][i]
		end
		
		if (B.data ~= v.data) then
			B.data = v.data
			B.Name:SetText(v.data.name)
			local tex = GetIconData(v.data.iconNum)
			B.Icon:Texture(tex)
			B:Show()
		end
		B.Timer:SetText(TimeUtils.FormatTimeCondensed(v.duration))
		B.Fill:Resize(B.width*v.duration/durationTable[v.data.name],20)
	end
	
	for j=i+1,maximum[typ] do
		if (F[typ][j]) then
			F[typ][j]:Hide()
		end
	end
	
	maximum[typ] = i
end

function AmethystEffects.Recreate()
	RecreateHook()
	
	for typ, _ in pairs(F) do
		for i, _ in pairs(F[typ]) do
			F[typ][i]:Destroy()
			F[typ][i] = nil
		end
		AmethystEffects.Render(typ)
	end
end

function AmethystEffects.ShowGUI()
	local s = AmethystEffects.Settings
	local I = GUI.Input
	local targetTypes = {"selffriendlytarget","selfhostiletarget"}
	
	for i,typ in pairs(targetTypes) do
		I["Enabled"..i]:SetValue(s[typ].enabled)
		I["Width"..i]:SetText(s[typ].width)
		I["Height"..i]:SetText(s[typ].height)
		I["Color"..i].Overlay:Tint(s[typ].color)
		I["Anchor"..i]:Select(s[typ].anchor)
		I["X"..i]:SetText(s[typ].x)
		I["Y"..i]:SetText(s[typ].y)
		I["Icon"..i]:Select(s[typ].iconpos)
	end
	GUI:Show()
end

function AmethystEffects.ApplySettings()
	local s = AmethystEffects.Settings
	local I = GUI.Input
	local targetTypes = {"selffriendlytarget","selfhostiletarget"}
	
	for i,typ in pairs(targetTypes) do
		s[typ].enabled = I["Enabled"..i]:GetValue()
		s[typ].width = tonumber(I["Width"..i]:GetText())
		s[typ].height = tonumber(I["Height"..i]:GetText())
		s[typ].color = {WindowGetTintColor(I["Color"..i].Overlay.name)}
		s[typ].anchor = WStringToString(I["Anchor"..i]:Selected())
		s[typ].x = tonumber(I["X"..i]:GetText())
		s[typ].y = tonumber(I["Y"..i]:GetText())
		s[typ].iconpos = WStringToString(I["Icon"..i]:Selected())
	end
	
	for typ,_ in pairs(F) do
		for k,v in pairs(F[typ]) do
			F[typ][k]:Destroy()
			F[typ][k] = nil
		end
	end
end

function AmethystEffects.CreateGUI()
	local AMT = CreateGUIHook()
	
	AMT.EffectsButton = AMT("Button")
	AMT.EffectsButton:Position(10,10)
	AMT.EffectsButton:SetText("AmethystEffects")
	AMT.EffectsButton:Resize(200);
	AMT.EffectsButton.OnLButtonUp =
		function()
			AmethystEffects.ShowGUI()
		end
		
	GUI = LibGUI("Blackframe")
	GUI:MakeMovable()
	GUI:AnchorTo("Root","center","center")
	GUI:Resize(800,500)
	GUI:Hide()
	
	GUI.CloseButton = GUI("Closebutton")
	GUI.CloseButton.OnLButtonUp =
		function()
			GUI:Hide()
		end

	GUI.Title = GUI("Label")
	GUI.Title:Resize(200)
	GUI.Title:AnchorTo(GUI, "top", "top", 0, 10)
	GUI.Title:Font("font_clear_large_bold")
	GUI.Title:SetText("AmethystEffects")
	GUI.Title:Color(153,50,204)
	GUI.Title:Align("center")
	GUI.Title:IgnoreInput()

	GUI.Apply = GUI("Button")
	GUI.Apply:Resize(250)
	GUI.Apply:SetText("Apply")
	GUI.Apply:AnchorTo(GUI,"bottom","bottomright",-20,-20)
	GUI.Apply.OnLButtonUp =
		function()
			AmethystEffects.ApplySettings()
		end
		
	GUI.Cancel = GUI("Button")
	GUI.Cancel:Resize(250)
	GUI.Cancel:SetText("Cancel")
	GUI.Cancel:AnchorTo(GUI,"bottom","bottomleft",20,-20)
	GUI.Cancel.OnLButtonUp =
		function()
			GUI:Hide()
		end
	
	GUI.Label = {}
	GUI.Input = {}
	
	GUI.Label.Title1 = GUI("Label")
	GUI.Label.Title1:Position(20,55)
	GUI.Label.Title1:Align("left")
	GUI.Label.Title1:SetText("friendly target buffs:")
 
	GUI.Input.Enabled1 = GUI("Checkbox")
	GUI.Input.Enabled1:Position(120,90)
	GUI.Label.Enabled1 = GUI("Label")
	GUI.Label.Enabled1:AnchorTo(GUI.Input.Enabled1, "right", "left", 15, 5)
	GUI.Label.Enabled1:Align("left")
	GUI.Label.Enabled1:SetText("enabled") 
	
	GUI.Input.Width1 = GUI("Textbox")
	GUI.Input.Width1:Resize(120)
	GUI.Input.Width1:Position(20,120)
	GUI.Label.Width1 = GUI("Label")
	GUI.Label.Width1:AnchorTo(GUI.Input.Width1, "right", "left", 15, 5)
	GUI.Label.Width1:Align("left")
	GUI.Label.Width1:SetText("width")
	
	GUI.Input.Height1 = GUI("Textbox")
	GUI.Input.Height1:Resize(120)
	GUI.Input.Height1:Position(20,160)
	GUI.Label.Height1 = GUI("Label")
	GUI.Label.Height1:AnchorTo(GUI.Input.Height1, "right", "left", 15, 5)
	GUI.Label.Height1:Align("left")
	GUI.Label.Height1:SetText("height")

	GUI.Input.Color1 = GUI("Button")
	GUI.Input.Color1:Resize(36,36)
	GUI.Input.Color1:Position(105,200)
	GUI.Input.Color1:Alpha(0)
	GUI.Input.Color1.Overlay = GUI("Image")
	GUI.Input.Color1.Overlay:Resize(36,36)
	GUI.Input.Color1.Overlay:Position(105,200)
	GUI.Input.Color1.Overlay:IgnoreInput()
	GUI.Label.Color1 = GUI("Label")
	GUI.Label.Color1:AnchorTo(GUI.Input.Color1, "right", "left", 15, 5)
	GUI.Label.Color1:Align("left")
	GUI.Label.Color1:SetText("color")
	GUI.Input.Color1.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.Color1.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end

	GUI.Input.Anchor1 = GUI("smallcombobox")
	GUI.Input.Anchor1:Position(20,240)
	GUI.Label.Anchor1 = GUI("Label")
	GUI.Label.Anchor1:AnchorTo(GUI.Input.Anchor1, "right", "left", 15, 5)
	GUI.Label.Anchor1:Align("left")
	GUI.Label.Anchor1:SetText("anchor")
	GUI.Input.Anchor1:Add("topleft")
	GUI.Input.Anchor1:Add("topright")
	GUI.Input.Anchor1:Add("bottomleft")
	GUI.Input.Anchor1:Add("bottomright")
	
	GUI.Input.X1 = GUI("Textbox")
	GUI.Input.X1:Resize(120)
	GUI.Input.X1:Position(20,280)
	GUI.Label.X1 = GUI("Label")
	GUI.Label.X1:AnchorTo(GUI.Input.X1, "right", "left", 15, 5)
	GUI.Label.X1:Align("left")
	GUI.Label.X1:SetText("offset x")	

	GUI.Input.Y1 = GUI("Textbox")
	GUI.Input.Y1:Resize(120)
	GUI.Input.Y1:Position(20,320)
	GUI.Label.Y1 = GUI("Label")
	GUI.Label.Y1:AnchorTo(GUI.Input.Y1, "right", "left", 15, 5)
	GUI.Label.Y1:Align("left")
	GUI.Label.Y1:SetText("offset y")
	
	GUI.Input.Icon1 = GUI("smallcombobox")
	GUI.Input.Icon1:Position(20,360)
	GUI.Label.Icon1 = GUI("Label")
	GUI.Label.Icon1:AnchorTo(GUI.Input.Icon1, "right", "left", 15, 5)
	GUI.Label.Icon1:Align("left")
	GUI.Label.Icon1:SetText("icon position")
	GUI.Input.Icon1:Add("none")
	GUI.Input.Icon1:Add("left")
	GUI.Input.Icon1:Add("right")

	GUI.Label.Title2 = GUI("Label")
	GUI.Label.Title2:Position(420,55)
	GUI.Label.Title2:Align("left")
	GUI.Label.Title2:SetText("enemy target buffs:")
 
	GUI.Input.Enabled2 = GUI("Checkbox")
	GUI.Input.Enabled2:Position(520,90)
	GUI.Label.Enabled2 = GUI("Label")
	GUI.Label.Enabled2:AnchorTo(GUI.Input.Enabled2, "right", "left", 15, 5)
	GUI.Label.Enabled2:Align("left")
	GUI.Label.Enabled2:SetText("enabled") 
	
	GUI.Input.Width2 = GUI("Textbox")
	GUI.Input.Width2:Resize(120)
	GUI.Input.Width2:Position(420,120)
	GUI.Label.Width2 = GUI("Label")
	GUI.Label.Width2:AnchorTo(GUI.Input.Width2, "right", "left", 15, 5)
	GUI.Label.Width2:Align("left")
	GUI.Label.Width2:SetText("width")
	
	GUI.Input.Height2 = GUI("Textbox")
	GUI.Input.Height2:Resize(120)
	GUI.Input.Height2:Position(420,160)
	GUI.Label.Height2 = GUI("Label")
	GUI.Label.Height2:AnchorTo(GUI.Input.Height2, "right", "left", 15, 5)
	GUI.Label.Height2:Align("left")
	GUI.Label.Height2:SetText("height")

	GUI.Input.Color2 = GUI("Button")
	GUI.Input.Color2:Resize(36,36)
	GUI.Input.Color2:Position(505,200)
	GUI.Input.Color2:Alpha(0)
	GUI.Input.Color2.Overlay = GUI("Image")
	GUI.Input.Color2.Overlay:Resize(36,36)
	GUI.Input.Color2.Overlay:Position(505,200)
	GUI.Input.Color2.Overlay:IgnoreInput()
	GUI.Label.Color2 = GUI("Label")
	GUI.Label.Color2:AnchorTo(GUI.Input.Color2, "right", "left", 15, 5)
	GUI.Label.Color2:Align("left")
	GUI.Label.Color2:SetText("color")
	GUI.Input.Color2.OnLButtonUp =
		function()
			GUI.Colorizer:Show()
			GUI.Colorizer.Object = GUI.Input.Color2.Overlay
			local r, g, b = WindowGetTintColor(GUI.Colorizer.Object.name)
			GUI.Colorizer.R:SetValue(r)
			GUI.Colorizer.G:SetValue(g)
			GUI.Colorizer.B:SetValue(b)
		end

	GUI.Input.Anchor2 = GUI("smallcombobox")
	GUI.Input.Anchor2:Position(420,240)
	GUI.Label.Anchor2 = GUI("Label")
	GUI.Label.Anchor2:AnchorTo(GUI.Input.Anchor2, "right", "left", 15, 5)
	GUI.Label.Anchor2:Align("left")
	GUI.Label.Anchor2:SetText("anchor")
	GUI.Input.Anchor2:Add("topleft")
	GUI.Input.Anchor2:Add("topright")
	GUI.Input.Anchor2:Add("bottomleft")
	GUI.Input.Anchor2:Add("bottomright")
	
	GUI.Input.X2 = GUI("Textbox")
	GUI.Input.X2:Resize(120)
	GUI.Input.X2:Position(420,280)
	GUI.Label.X2 = GUI("Label")
	GUI.Label.X2:AnchorTo(GUI.Input.X2, "right", "left", 15, 5)
	GUI.Label.X2:Align("left")
	GUI.Label.X2:SetText("offset x")	

	GUI.Input.Y2 = GUI("Textbox")
	GUI.Input.Y2:Resize(120)
	GUI.Input.Y2:Position(420,320)
	GUI.Label.Y2 = GUI("Label")
	GUI.Label.Y2:AnchorTo(GUI.Input.Y2, "right", "left", 15, 5)
	GUI.Label.Y2:Align("left")
	GUI.Label.Y2:SetText("offset y")
	
	GUI.Input.Icon2 = GUI("smallcombobox")
	GUI.Input.Icon2:Position(420,360)
	GUI.Label.Icon2 = GUI("Label")
	GUI.Label.Icon2:AnchorTo(GUI.Input.Icon2, "right", "left", 15, 5)
	GUI.Label.Icon2:Align("left")
	GUI.Label.Icon2:SetText("icon position")
	GUI.Input.Icon2:Add("none")
	GUI.Input.Icon2:Add("left")
	GUI.Input.Icon2:Add("right")

	GUI.Colorizer = GUI("Blackframe")
	GUI.Colorizer:Resize(250,250)
	GUI.Colorizer:Position(250,200)
	GUI.Colorizer:Hide()
	
	GUI.Colorizer.RLabel = GUI.Colorizer("Label")
	GUI.Colorizer.RLabel:SetText("red")
	GUI.Colorizer.RLabel:Position(20,0)
	
	GUI.Colorizer.R = GUI.Colorizer("Slider")
	GUI.Colorizer.R:Position(20,20)
	GUI.Colorizer.R:SetRange(0,255)
	
	GUI.Colorizer.GLabel = GUI.Colorizer("Label")
	GUI.Colorizer.GLabel:SetText("green")
	GUI.Colorizer.GLabel:Position(20,60)
	
	GUI.Colorizer.G = GUI.Colorizer("Slider")
	GUI.Colorizer.G:Position(20,80)
	GUI.Colorizer.G:SetRange(0,255)

	GUI.Colorizer.BLabel = GUI.Colorizer("Label")
	GUI.Colorizer.BLabel:SetText("blue")
	GUI.Colorizer.BLabel:Position(20,120)
	
	GUI.Colorizer.B = GUI.Colorizer("Slider")
	GUI.Colorizer.B:Position(20,140)
	GUI.Colorizer.B:SetRange(0,255)
	
	GUI.Colorizer.Okay = GUI.Colorizer("Button")
	GUI.Colorizer.Okay:SetText("Okay")
	GUI.Colorizer.Okay:Resize(200)
	GUI.Colorizer.Okay:AnchorTo(GUI.Colorizer, "bottom", "bottom", 0, -20)
	GUI.Colorizer.Okay.OnLButtonUp =
		function()
			GUI.Colorizer:Hide()
		end
	
	for k,v in pairs(GUI.Label) do
		v:IgnoreInput()
	end


end